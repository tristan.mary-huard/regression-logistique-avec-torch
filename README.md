# Regression logistique avec torch


Ce répertoire contient 3 fichiers .R correspondant à la partie pratique de l'atelier "Différentiation automatique et optimisation à l'aide du package torch", réalisé dans le cadre des 10emes Rencontres R (Vannes, 2024).

Il contient 3 fichiers .R:

- **1_RegressionLogistique.R** pour la mise en place de l'optimisation avec torch,  

- **2_LBFGS.R** pour l'usage particulier de la procédure d'optimisation L-BFGS,

- **3_MiniBatch.R** pour une implémentation simple de l'apprentissage par batch.